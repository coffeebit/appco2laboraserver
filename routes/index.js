var express = require('express');
var router = express.Router();
var DBModel = require('../models/db/dbmodels');

router.get('/testconnection', function(req, res, next) {
  //creamos un objeto con los datos a insertar del usuario
    var userData = {
    	email : req.body.email,
        password : req.body.password
    };

    DBModel.testConnection(function(error, data){
    	res.json(200,data);
    });
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/signUp', function(req, res, next) {
  //creamos un objeto con los datos a insertar del usuario
  	console.log(req.body);
  	req.body['dateRegister'] = getDateTime();
  	req.body['id'] = Date.now();
  	
    DBModel.checkUser(req.body,function(error, data){
    	if(data.data.success){
    		console.log("entre");
    		DBModel.insertUser(req.body,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.post('/signIn', function(req, res, next) {
    DBModel.signIn(req.body,function(error, data){
    	res.json(200,data);
    });
});

//REMINDERS//
router.get('/:id/reminders', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getReminders(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.post('/:id/reminders', function(req, res) {
	req.body["dateRecord"] = getDateTime();
	req.body["idUser"] = req.params.id;
	req.body['id'] = Date.now();
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.addReminder(req.body,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.put('/:id/reminders/:idReminder', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.editReminder(req.body,req.params.idReminder,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.delete('/:id/reminders/:idReminder', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.deleteReminder(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

//SETTINGS//
router.get('/:id/settings', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getSettings(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.post('/:id/settings', function(req, res) {
	req.body["dateRecord"] = getDateTime();
	req.body["idUser"] = req.params.id;
	req.body['id'] = Date.now();
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.addSettings(req.body,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.put('/:id/settings/:idSettings', function(req, res) {
	console.log(req.params);
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.editSettings(req.body,req.params.idSettings,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.delete('/:id/settings/:idSettings', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.deleteSettings(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

//PROFILE//
router.get('/:id/profile', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getProfile(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.post('/:id/profile', function(req, res) {
	req.body["dateRecord"] = getDateTime();
	req.body["idUser"] = req.params.id;
	req.body['id'] = Date.now();
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.addProfile(req.body,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.put('/:id/profile/:idProfile', function(req, res) {
	console.log(req.params);
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.editProfile(req.body,req.params.idProfile,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.delete('/:id/profile/:idProfile', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.deleteProfile(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

//ranking//
router.get('/:id/ranking', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getRankings(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.get('/:id/ranking/:idRanking', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getRanking(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.post('/:id/ranking', function(req, res) {
	req.body["dateRecord"] = getDateTime();
	req.body["idUser"] = req.params.id;
	req.body['id'] = Date.now();
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.addRanking(req.body,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.put('/:id/ranking/:idRanking', function(req, res) {
	console.log(req.params);
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.editRanking(req.body,req.params.idRanking,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.delete('/:id/ranking/:idRanking', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.deleteRanking(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

//Ranking//
router.get('/:id/ranking', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getRankings(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.get('/:id/ranking/:idRanking', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.getRanking(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.post('/:id/ranking', function(req, res) {
	req.body["dateRecord"] = getDateTime();
	req.body["idUser"] = req.params.id;
	req.body['id'] = Date.now();
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.addRanking(req.body,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.put('/:id/ranking/:idRanking', function(req, res) {
	console.log(req.params);
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.editRanking(req.body,req.params.idRanking,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});

router.delete('/:id/ranking/:idRanking', function(req, res) {
	DBModel.validId(req.params,function(error, data){
    	if(data.data.success){
    		DBModel.deleteRanking(req.params,function(error, data){
    			res.json(200,data);
    		});
    	}else{
    		res.json(200,data);
    	}
    });
});
function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "-" + month + "-" + day + " " + hour + ":" + min + ":" + sec;
}

module.exports = router;
