# README #

This README explain how use this server

### Requirements ###

* node installed

### How install? ###

* terminal

* git clone https://c35xr@bitbucket.org/coffeebit/appco2laboraserver.git

* npm install 

### How start server ###

* npm start // default port is :3030, check http://localhost:3030

### API Usage ###

NOTE

to use the API is not necessary to install the server

# header #

Content-Type application/json

# body #

raw

### Base URL ###

http://coffeebit.us:3030

# SINGUP #

### Register URL ###

/singUp 

method post, signup to app

```
	{
		email : "buenrostro@prueba.com",
		password : "123",
		userFb : 0, //0 false, 1 true
		username : "c35xr" //optional
		idFb: "xoigebos8363osoi3630", //optional
		confirmate : 1, //0 false, 1 true
	}
```

### Response ###

if email exists, returns

```
	{
	  "data": {
	    "tag": "checkUser",
	    "success": false,
	    "error": null,
	    "email_status": true,
	    "code": 400,
	    "msg": "El correo no esta disponible"
	  }
	}
```

if email not exists, returns

```
	{
	  "data": {
	    "tag": "register",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "user": [
	      {
	        "id": 1452765544205,
	        "username": null,
	        "email": "algo2@algo.com",
	        "password": "123",
	        "userFb": 0,
	        "dateRegister": "2016-01-14T09:59:04.000Z",
	        "confirmate": 1,
	        "idFb": null
	      }
	    ]
	  }
	}
```


# SIGNIN #

### Login URL ###

/singIn 

method post, signin to app

```
	{
		email : "buenrostro@prueba.com",
		password : "123",
	}
```

### Response ###

if email or pass, not exists or are incorrect, returns

```
	{
	  "data": {
	    "tag": "login",
	    "success": false,
	    "error": true,
	    "msg": "Datos incorrectos",
	    "code": 400
	  }
	}
```

if email and pass are correct, returns

```
	{
	  "data": {
	    "tag": "login",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "user": [
	      {
	        "id": 1452715425359,
	        "username": null,
	        "email": "algo@algo.com",
	        "password": "123",
	        "userFb": 0,
	        "dateRegister": "2016-01-13T20:03:45.000Z",
	        "confirmate": 1,
	        "idFb": null
	      }
	    ]
	  }
	}
```

# REMINDERS #

### Reminders URL ###

/:id/reminders

method get, get all reminders

### Response ###


if no reminders, returns

```
	{
	  "data": {
	    "tag": "getReminders",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "reminders not found"
	  }
	}
```

if there are reminders, returns

```
	{
	  "data": {
	    "tag": "getReminders",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "reminders": [
	      {
	        "id": 1452726561250,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-13T23:09:21.000Z",
	        "descriptionReminder": "Apagar la tv",
	        "timeReminder": "16:30:00",
	        "active": 0
	      },
	      {
	        "id": 1452728431769,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-13T23:40:31.000Z",
	        "descriptionReminder": "Apagar la cosa",
	        "timeReminder": "16:30:00",
	        "active": 1
	      },
	      .
	      .
	    ]
	  }
	}
```

### Reminders URL ###

/:id/reminders

method post, add reminder

```
	{
		descriptionReminder : "Apagar algo", //max 100 
		timeReminder : "16:30:00", //24 hours format
		active : 0 //0 false, 1 true
	}
```

### Response ###


if add reminders ok, returns

```
	{
	  "data": {
	    "tag": "addReminder",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "reminder": [
	      {
	        "id": 1452766061805,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-14T10:07:41.000Z",
	        "descriptionReminder": "Apagar la luz",
	        "timeReminder": "16:30:00",
	        "active": 1
	      }
	    ]
	  }
	}
```

### Reminders URL ###

/:id/reminders/:idReminder

method put, update reminder

```
	{
		descriptionReminder : "Apagar algo", //max 100 
		timeReminder : "16:30:00", //24 hours format
		active : 0 //0 false, 1 true
	}
```

### Response ###


if reminder exists, returns

```
	{
	  "data": {
	    "tag": "updateReminder",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "reminder": [
	      {
	        "id": 1452728431769,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-13T23:40:31.000Z",
	        "descriptionReminder": "Apagar la luz",
	        "timeReminder": "16:30:00",
	        "active": 1
	      }
	    ]
	  }
	}
```

if reminder no exists, returns

```
	{
	  "data": {
	    "tag": "updateReminder",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "reminder not found"
	  }
	}
```

### Reminders URL ###

/:id/reminders/:idReminder

method delete, delete reminder

### Response ###


if reminder exists, returns

```
	{
	  "data": {
	    "tag": "deleteReminder",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "msg": "reminder deleted"
	  }
	}
```

if reminder no exists, returns

```
	{
	  "data": {
	    "tag": "deleteReminder",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "reminder not found"
	  }
	}
```

# SETTINGS #

### Settings URL ###

/:id/settings

method get, get all settings

### Response ###


if settings exists, returns

```
	{
	  "data": {
	    "tag": "getSettings",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "settings": [
	      {
	        "id": 1452766540371,
	        "idUser": 1452715425359,
	        "reminders": 0,
	        "notifications": 1,
	        "tips": 1,
	        "dateRecord": "2016-01-14T10:15:40.000Z",
	        "takeTest": 7
	      }
	    ]
	  }
	}
```

if settings no exists, returns

```
	{
	  "data": {
	    "tag": "getSettings",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "settings not found"
	  }
	}
```


### Settings URL ###

/:id/settings

method post, add Settings

```
	{
		reminders : 0, //0 false, 1 true
		tips : 0, //0 false, 1 true
		notifications : 0 //0 false, 1 true
		takeTest : 7 //días
	}
```

### Response ###


if add settings ok, returns

```
	{
	  "data": {
	    "tag": "addSettings",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "settings": [
	      {
	        "id": 1452766540371,
	        "idUser": 1452715425359,
	        "reminders": 0,
	        "notifications": 1,
	        "tips": 1,
	        "dateRecord": "2016-01-14T10:15:40.000Z",
	        "takeTest": 7
	      }
	    ]
	  }
	}
```

### Settings URL ###

/:id/settings/:idSettings

method put, update Settings

```
	{
		reminders : 0, //0 false, 1 true
		tips : 0, //0 false, 1 true
		notifications : 0 //0 false, 1 true
		takeTest : 7 //días
	}
```

### Response ###


if settings exists, returns

```
	{
	  "data": {
	    "tag": "updateSettings",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "settings": [
	      {
	        "id": 1452766540371,
	        "idUser": 1452715425359,
	        "reminders": 0,
	        "notifications": 1,
	        "tips": 1,
	        "dateRecord": "2016-01-14T10:15:40.000Z",
	        "takeTest": 30
	      }
	    ]
	  }
	}
```

if settings no exists, returns

```
	{
	  "data": {
	    "tag": "updateSettings",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Settings not found"
	  }
	}
```


### settings URL ###

/:id/settings/:idSettings

method delete, delete Settings

### Response ###


if settings exists, returns

```
	{
	  "data": {
	    "tag": "deleteSettings",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "msg": "settings deleted"
	  }
	}
```

if settings no exists, returns

```
	{
	  "data": {
	    "tag": "deleteSettings",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "settings not found"
	  }
	}
```


# PROFILE #

### Profile URL ###

/:id/profile

method get, get Profile

### Response ###


if profile exists, returns

```
	{
	  "data": {
	    "tag": "getProfile",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "profile": [
	      {
	        "id": 1452771021353,
	        "iduser": 1452715425359,
	        "name": "César Iván Buenrostro Segura",
	        "address": "calle colonia y mas datos",
	        "picture": "url_de_la_imagen",
	        "dateRecord": "2016-01-14T11:30:21.000Z"
	      }
	    ]
	  }
	}
```

if profile no exists, returns

```
	{
	  "data": {
	    "tag": "getProfile",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "profile not found"
	  }
	}
```


### Profile URL ###

/:id/profile

method post, add Profile

```
	{
		name : "César Iván Buenrostro Segura", //max 45 
		address : "calle numero colonia ciudad estado", //max 200
		picture : "url_de_la_imagen" //max 300
	}
```

### Response ###


if add profile ok, returns

```
	{
	  "data": {
	    "tag": "addProfile",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "profile": [
	      {
	        "id": 1452771021353,
	        "iduser": 1452715425359,
	        "name": "César Iván Buenrostro Segura",
	        "address": "calle colonia y mas datos",
	        "picture": "url_de_la_imagen",
	        "dateRecord": "2016-01-14T11:30:21.000Z"
	      }
	    ]
	  }
	}
```


### Profile URL ###

/:id/profile/:idProfile

method put, update Profile

```
	{
		name : "César Iván Buenrostro Segura", //max 45 
		address : "calle numero colonia ciudad estado", //max 200
		picture : "url_de_la_imagen" //max 300
	}
```

### Response ###


if profile exists, returns

```
	{
	  "data": {
	    "tag": "updateProfile",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "profile": [
	      {
	        "id": 1452771021353,
	        "iduser": 1452715425359,
	        "name": "Cesar Ivan Buenrostro Segura",
	        "address": "calle colonia y mas datos",
	        "picture": "url_de_la_imagen",
	        "dateRecord": "2016-01-14T11:30:21.000Z"
	      }
	    ]
	  }
	}
```

if profile no exists, returns

```
	{
	    "data": {
	      "tag": "updateProfile",
	      "success": false,
	      "error": true,
	      "code": 400,
	      "msg": "Profile not found"
	    }
	}
```


### Profile URL ###

/:id/profile/:idProfile

method delete, delete Profile

### Response ###

if profile exists, returns

```
	{
	  "data": {
	    "tag": "deleteProfile",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "msg": "Profile deleted"
	  }
	}
```

if profile no exists, returns

```
	{
	  "data": {
	    "tag": "deleteProfile",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Profile not found"
	  }
	}
```

# TEST #

### Test URL ###

/:id/test

method get, get all Tests

### Response ###


if tests exists, returns

```
	{
	  "data": {
	    "tag": "getTests",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "test": [
	      {
	        "id": 1452756858142,
	        "idUser": 1452715425359,
	        "value1": 80,
	        "value1Shared": 1,
	        "value2": 35,
	        "value2Shared": 1,
	        "value3": 25,
	        "value3Shared": 1,
	        "value4": 60,
	        "value4Shared": 1,
	        "dateRecord": "2016-01-14T07:34:18.000Z"
	      },
	      {
	        "id": 1452756855169,
	        "idUser": 1452715425359,
	        "value1": 90,
	        "value1Shared": 1,
	        "value2": 75,
	        "value2Shared": 1,
	        "value3": 45,
	        "value3Shared": 1,
	        "value4": 80,
	        "value4Shared": 1,
	        "dateRecord": "2016-01-14T09:18:05.000Z"
	      }
	    ]
	  }
	}
```

if tests no exists, returns

```
	{
	  "data": {
	    "tag": "getTests",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Test not found"
	  }
	}
```

### Test URL ###

/:id/test/:idTest

method get, get specific Test

### Response ###


if specific test exists, returns

```
	{
	  "data": {
	    "tag": "getTest",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "test": [
	      {
	        "id": 1452756855169,
	        "idUser": 1452715425359,
	        "value1": 90,
	        "value1Shared": 1,
	        "value2": 75,
	        "value2Shared": 1,
	        "value3": 45,
	        "value3Shared": 1,
	        "value4": 80,
	        "value4Shared": 1,
	        "dateRecord": "2016-01-14T09:18:05.000Z"
	      }
	    ]
	  }
	}
```

if tests no exists, returns

```
	{
	  "data": {
	    "tag": "getTest",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Test not found"
	  }
	}
```

### Test URL ###

/:id/test

method post, add Test

```
	{
		value1 : 100, //Light
		value1Shared : 1, //number of people
		value2 : 100, //Gasoline
		value2Shared : 1, //number of people
		value3 : 100, //Gas
		value3Shared : 1, //number of people
		value4 : 100, //Recycle
		value4Shared : 1, //number of people
	}
```

### Response ###


if add test ok, returns

```
	{
	  "data": {
	    "tag": "addTest",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "test": [
	      {
	        "id": 1452801077679,
	        "idUser": 1452715425359,
	        "value1": 80,
	        "value1Shared": 1,
	        "value2": 35,
	        "value2Shared": 1,
	        "value3": 25,
	        "value3Shared": 1,
	        "value4": 60,
	        "value4Shared": 1,
	        "dateRecord": "2016-01-14T19:51:17.000Z"
	      }
	    ]
	  }
	}
```



### Test URL ###

/:id/test/:idTest

method put, update Test

```
	{
		value1 : 100, //Light
		value1Shared : 1, //number of people
		value2 : 100, //Gasoline
		value2Shared : 1, //number of people
		value3 : 100, //Gas
		value3Shared : 1, //number of people
		value4 : 100, //Recycle
		value4Shared : 1, //number of people
	}
```

### Response ###

if test exists, returns

```
	{
	  "data": {
	    "tag": "updateTest",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "test": [
	      {
	        "id": 1452801077679,
	        "idUser": 1452715425359,
	        "value1": 80,
	        "value1Shared": 1,
	        "value2": 35,
	        "value2Shared": 1,
	        "value3": 25,
	        "value3Shared": 1,
	        "value4": 60,
	        "value4Shared": 1,
	        "dateRecord": "2016-01-14T19:51:17.000Z"
	      }
	    ]
	  }
	}
```

if test no exists, returns

```
	{
	  "data": {
	    "tag": "updateTest",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Test not found"
	  }
	}
```

### Test URL ###

/:id/test/:idTest

method delete, delete Test

### Response ###

if test exists, returns

```
	{
	  "data": {
	    "tag": "deleteTest",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "msg": "Test deleted"
	  }
	}
```

if test no exists, returns

```
	{
	  "data": {
	    "tag": "deleteTest",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Test not found"
	  }
	}
```

# RANKING #

### Ranking URL ###

/:id/ranking

method get, get all Rankings

### Response ###


if rankings exists, returns

```
	{
	  "data": {
	    "tag": "getRankings",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "rankings": [
	      {
	        "id": 1452819849793,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-15T01:04:09.000Z",
	        "value": "205.08"
	      },
	      {
	        "id": 1452819849793,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-15T01:04:09.000Z",
	        "value": "205.08"
	      },
	      .
	      .
	    ]
	  }
	}

```

if rankings no exists, returns

```
	{
	  "data": {
	    "tag": "getRankings",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Rankings not found"
	  }
	}
```

### Ranking URL ###

/:id/ranking/:idRanking

method get, get specific Ranking

### Response ###


if specific ranking exists, returns

```
	{
	  "data": {
	    "tag": "getRanking",
	    "success": true,
	    "error": false,
	    "code": 200,
	    "Ranking": [
	      {
	        "id": 1452819849793,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-15T01:04:09.000Z",
	        "value": "205.08"
	      }
	    ]
	  }
	}

```

if specific ranking no exists, returns

```
	{
	  "data": {
	    "tag": "getRanking",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Ranking not found"
	  }
	}
```

### Ranking URL ###

/:id/ranking

method post, add Ranking

```
	{
		value : "205.08"
	}
```

### Response ###


if add ranking ok, returns

```
	{
	  "data": {
	    "tag": "addRanking",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "ranking": [
	      {
	        "id": 1452819849793,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-15T01:04:09.000Z",
	        "value": "205.08"
	      }
	    ]
	  }
	}
```

### Ranking URL ###

/:id/ranking/:idRanking

method put, update Ranking

```
	{
		value : "145.08"
	}
```

### Response ###

if ranking exists, returns

```
	{
	  "data": {
	    "tag": "updateRanking",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "ranking": [
	      {
	        "id": 1452819849793,
	        "idUser": 1452715425359,
	        "dateRecord": "2016-01-15T01:04:09.000Z",
	        "value": "305.08"
	      }
	    ]
	  }
	}
```

if ranking no exists, returns

```
	{
	  "data": {
	    "tag": "updateRanking",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Ranking not found"
	  }
	}
```

### Ranking URL ###

/:id/ranking/:idRanking

method delete, delete Ranking

### Response ###

if ranking exists, returns

```
	{
	  "data": {
	    "tag": "deleteRanking",
	    "success": true,
	    "error": false,
	    "code": 201,
	    "msg": "Ranking deleted"
	  }
	}
```

if ranking no exists, returns

```
	{
	  "data": {
	    "tag": "deleteRanking",
	    "success": false,
	    "error": true,
	    "code": 400,
	    "msg": "Ranking not found"
	  }
	}
```