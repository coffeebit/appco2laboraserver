//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: 'mysql.laria.rasoft.us', 
        user: 'lariauser',  
        password: 'lariapassword', 
        database: 'lariadb',
        multipleStatements: true
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var lariaModel = {};

//Comprobamos conexion
lariaModel.testConnection = function(callback)
{
    if (connection) 
    {
        callback(null,{"msg":"Conectado"});
    }else{
        callback(null,{"msg":"Desconectado"});
    }
}

//añadir un nuevo usuario
lariaModel.insertUser = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO users SET ?', userData, function(error, result) 
        {
            if(error)
            {
                throw error;
            }
            else
            {   
                jsonresponse = {
                            tag : "register",
                            success : true,
                            error : false
                };
                jsonresponse["uid"] = result.insertId;

                jsonresponse2 = {
                    cloudsalud : []
                };

                connection.query('SELECT * FROM users WHERE id_user = '+ result.insertId, function(error, result) 
                {
                    if(error)
                    {
                        throw error;
                    }
                    else{
                        
                        
                        jsonresponse["user"] = result;
                        //devolvemos la última id insertada
                        jsonresponse2["cloudsalud"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });

            }
        });
    }
}

//verificar un usuario pasando el email o username
lariaModel.checkUser = function(userData, callback)
{
    respuesta = {
        email : 0,
        username : 0
    };

    jsonresponse2 = {
        cloudsalud : []
    };

    if(connection)
    {
        var sqlExists = 'SELECT * FROM users WHERE email = ' + connection.escape(userData.email);
        connection.query(sqlExists, function(err, row) 
        {
            //si existe el email del usuario 
            if(row.length != 0)
            {
                respuesta["email"] = 1;
                var sql = 'SELECT * FROM users WHERE username = ' + connection.escape(userData.username);
                connection.query(sql, function(error, result) 
                {
                    if(result.length != 0)
                    {
                        jsonresponse = {
                            tag : "register",
                            success : false,
                            error : true,
                            email_status : true,
                            username_status : true,
                            error_msg : "El correo y nombre de usuario no esta disponible"
                        };
                        
                        respuesta["username"] = 1;
                        jsonresponse2["cloudsalud"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }else{

                        jsonresponse = {
                            tag : "register",
                            success : false,
                            error : true,
                            email_status : true,
                            username_status : false,
                            error_msg : "El correo no esta disponible"
                        };
                        jsonresponse2["cloudsalud"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }
                });
            }
            else
            {
                var sql = 'SELECT * FROM users WHERE username = ' + connection.escape(userData.username);
                connection.query(sql, function(error, result) 
                {
                    if(result.length != 0)
                    {
                        respuesta["username"] = 1;
                        jsonresponse = {
                            tag : "register",
                            success : false,
                            error : true,
                            email_status : false,
                            username_status : true,
                            error_msg : "El nombre de usuario no esta disponible"
                        };
                        jsonresponse2["cloudsalud"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }else{
                        jsonresponse = {
                            tag : "register",
                            success : true,
                            error : false,
                            email_status : false,
                            username_status : false
                        };
                        jsonresponse2["cloudsalud"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }
                });
            }
        });
    }
}

//eliminar un usuario pasando la id a eliminar
lariaModel.loginUser = function(userData, callback)
{

    if(connection)
    {
        var sqlExists = 'SELECT * FROM users WHERE (email = ' + connection.escape(userData.user_email) + ' OR username = ' + connection.escape(userData.user_email) + ')  AND password  = ' + connection.escape(userData.password);
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                cloudsalud : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "login",
                    success : true,
                    error : false
                };
                jsonresponse["user"] = row;
                jsonresponse2["cloudsalud"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "login",
                    success : false,
                    error : true,
                    msg : "Error de login"
                };
                jsonresponse2["cloudsalud"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = lariaModel;