//llamamos al paquete mysql que hemos instalado
var mysql = require('mysql'),
//creamos la conexion a nuestra base de datos con los datos de acceso de cada uno
connection = mysql.createConnection(
    { 
        host: '45.56.94.219', 
        user: 'co2',  
        password: '279012cesar', 
        database: 'co2labora',
        multipleStatements: true
    }
);
 
//creamos un objeto para ir almacenando todo lo que necesitemos
var dbModel = {};

//Comprobamos conexion
dbModel.testConnection = function(callback)
{
    if (connection) 
    {
        callback(null,{"msg":"Conectado"});
    }else{
        callback(null,{"msg":"Desconectado"});
    }
}

//añadir un nuevo usuario
dbModel.insertUser = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO users SET ?', userData, function(err, row) 
        {

            if(row.length == 0)
            {
                throw err;
            }
            else
            {   
                jsonresponse = {
                    tag : "register",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM users WHERE id = '+ userData.id, function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        
                        
                        jsonresponse["user"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });

            }
        });
    }
}

//verificar un usuario pasando el email o username
dbModel.checkUser = function(userData, callback)
{
    jsonresponse2 = {
        data : []
    };

    if(connection)
    {
        var sqlExists = 'SELECT * FROM users WHERE email = ' + connection.escape(userData.email);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "checkUser",
                    success : false,
                    error : err,
                    email_status : true,
                    code : 400,
                    msg : "El correo no esta disponible"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {
                jsonresponse = {
                    tag : "checkUser",
                    success : true,
                    error : false,
                    email_status : false,
                    code : 200,
                    msg : "El correo esta disponible"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//verificar un usuario pasando el email o username
dbModel.validId = function(userData, callback)
{
    jsonresponse2 = {
        data : []
    };

    if(connection)
    {
        console.log(userData.id);
        var sqlExists = 'SELECT * FROM users WHERE id = ' + connection.escape(userData.id);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "validId",
                    success : true,
                    error : false,
                    code : 200,
                    msg : "Usuario encontrado"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {
                jsonresponse = {
                    tag : "validId",
                    success : false,
                    error : err,
                    code : 400,
                    msg : "Usuario no encontrado"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//eliminar un usuario pasando la id a eliminar
dbModel.signIn = function(userData, callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM users WHERE email = ' + connection.escape(userData.email) + ' AND password  = ' + connection.escape(userData.password);
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "login",
                    success : true,
                    error : false,
                    code : 200,
                };
                jsonresponse["user"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "login",
                    success : false,
                    error : true,
                    msg : "Datos incorrectos",
                    code : 400,
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get reminders
dbModel.getReminders = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM reminders WHERE idUser = '+ userData.id;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getReminders",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["reminders"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getReminders",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "reminders not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//add reminder
dbModel.addReminder = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO reminders SET ?', userData, function(err, row) 
        {

            if(row.length == 0)
            {
                throw err;
            }
            else
            {   
                jsonresponse = {
                    tag : "addReminder",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM reminders WHERE id = '+ userData.id, function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["reminder"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });
            }
        });
    }
}

//edit reminder
dbModel.editReminder = function(userData,idReminder,callback)
{
    if (connection) 
    {
        var sqlExists = 'UPDATE reminders SET ? WHERE id = ' + connection.escape(idReminder);
        console.log(sqlExists);
        connection.query(sqlExists,userData, function(err, row) 
        {
            
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "updateReminder",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "reminder not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "updateReminder",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM reminders WHERE id = '+ connection.escape(idReminder), function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["reminder"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });
            }
        });
    }
}

//delete reminder
dbModel.deleteReminder = function(userData,callback)
{
    if (connection) 
    {
        var sqlExists = 'DELETE FROM reminders WHERE id = ' + connection.escape(userData.idReminder);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "deleteReminder",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "reminder not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "deleteReminder",
                    success : true,
                    error : false, 
                    code : 201,
                    msg : "reminder deleted"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get settings
dbModel.getSettings = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM settings WHERE idUser = '+ userData.id;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getSettings",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["settings"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getSettings",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "settings not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//add settings
dbModel.addSettings = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO settings SET ?', userData, function(err, row) 
        {

            if(row.length == 0)
            {
                throw err;
            }
            else
            {   
                jsonresponse = {
                    tag : "addSettings",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM settings WHERE id = '+ userData.id, function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["settings"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }
                });
            }
        });
    }
}

//edit settings
dbModel.editSettings = function(userData,idSettings,callback)
{
    if (connection) 
    {
        var sqlExists = 'UPDATE settings SET ? WHERE id = ' + connection.escape(idSettings);
        console.log(sqlExists);
        connection.query(sqlExists,userData, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "updateSettings",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Settings not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "updateSettings",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM settings WHERE id = '+ connection.escape(idSettings), function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["settings"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });
            }
        });
    }
}

//delete settings
dbModel.deleteSettings = function(userData,callback)
{
    if (connection) 
    {
        var sqlExists = 'DELETE FROM settings WHERE id = ' + connection.escape(userData.idSettings);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "deleteSettings",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "settings not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "deleteSettings",
                    success : true,
                    error : false, 
                    code : 201,
                    msg : "settings deleted"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get profile
dbModel.getProfile = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM profile WHERE idUser = '+ userData.id;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getProfile",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["profile"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getProfile",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "profile not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//add profile
dbModel.addProfile = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO profile SET ?', userData, function(err, row) 
        {

            if(row.length == 0)
            {
                throw err;
            }
            else
            {   
                jsonresponse = {
                    tag : "addProfile",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM profile WHERE id = '+ userData.id, function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["profile"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }
                });
            }
        });
    }
}

//edit profile
dbModel.editProfile = function(userData,idProfile,callback)
{
    if (connection) 
    {
        var sqlExists = 'UPDATE profile SET ? WHERE id = ' + connection.escape(idProfile);
        console.log(sqlExists);
        connection.query(sqlExists,userData, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "updateProfile",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Profile not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "updateProfile",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM profile WHERE id = '+ connection.escape(idProfile), function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["profile"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });
            }
        });
    }
}

//delete profile
dbModel.deleteProfile = function(userData,callback)
{
    if (connection) 
    {
        var sqlExists = 'DELETE FROM profile WHERE id = ' + connection.escape(userData.idProfile);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "deleteProfile",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Profile not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "deleteProfile",
                    success : true,
                    error : false, 
                    code : 201,
                    msg : "Profile deleted"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get Test
dbModel.getTest = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM test WHERE idUser = '+ userData.id +' AND id = '+ userData.idTest;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getTest",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["test"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getTest",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "Test not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get Tests
dbModel.getTests = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM test WHERE idUser = '+ userData.id;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getTests",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["test"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getTests",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "Test not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//add Test
dbModel.addTest = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO test SET ?', userData, function(err, row) 
        {

            if(row.length == 0)
            {
                throw err;
            }
            else
            {   
                jsonresponse = {
                    tag : "addTest",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM test WHERE id = '+ userData.id, function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["test"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }
                });
            }
        });
    }
}

//edit Test
dbModel.editTest = function(userData,idTest,callback)
{
    if (connection) 
    {
        var sqlExists = 'UPDATE test SET ? WHERE id = ' + connection.escape(idTest);
        console.log(sqlExists);
        connection.query(sqlExists,userData, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "updateTest",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Test not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "updateTest",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM test WHERE id = '+ connection.escape(idTest), function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["test"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });
            }
        });
    }
}

//delete Test
dbModel.deleteTest = function(userData,callback)
{
    if (connection) 
    {
        var sqlExists = 'DELETE FROM test WHERE id = ' + connection.escape(userData.idTest);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "deleteTest",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Test not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "deleteTest",
                    success : true,
                    error : false, 
                    code : 201,
                    msg : "Test deleted"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get Ranking
dbModel.getRanking = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM ranking WHERE idUser = '+ userData.id +' AND id = '+ userData.idRanking;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getRanking",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["Ranking"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getRanking",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "Ranking not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//get Rankings
dbModel.getRankings = function(userData,callback)
{
    if(connection)
    {
        var sqlExists = 'SELECT * FROM ranking WHERE idUser = '+ userData.id;
        connection.query(sqlExists, function(err, row) 
        {
            jsonresponse2 = {
                data : []
            };
            //si existe el email del usuario 
            if(row.length != 0)
            {
                jsonresponse = {
                    tag : "getRankings",
                    success : true,
                    error : false,
                    code : 200
                };
                jsonresponse["rankings"] = row;
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);

            }
            else
            {
                jsonresponse = {
                    tag : "getRankings",
                    success : false,
                    error : true,
                    code : 400,
                    msg : "Rankings not found"
                };
                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}

//add Ranking
dbModel.addRanking = function(userData,callback)
{
    if (connection) 
    {
        connection.query('INSERT INTO ranking SET ?', userData, function(err, row) 
        {

            if(row.length == 0)
            {
                throw err;
            }
            else
            {   
                jsonresponse = {
                    tag : "addRanking",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM ranking WHERE id = '+ userData.id, function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["ranking"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);
                    }
                });
            }
        });
    }
}

//edit Ranking
dbModel.editRanking = function(userData,idRanking,callback)
{
    if (connection) 
    {
        var sqlExists = 'UPDATE ranking SET ? WHERE id = ' + connection.escape(idRanking);
        console.log(sqlExists);
        connection.query(sqlExists,userData, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "updateRanking",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Ranking not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "updateRanking",
                    success : true,
                    error : false, 
                    code : 201
                };

                jsonresponse2 = {
                    data : []
                };

                connection.query('SELECT * FROM ranking WHERE id = '+ connection.escape(idRanking), function(err, row) 
                {
                    if(err)
                    {
                        throw err;
                    }
                    else{
                        jsonresponse["ranking"] = row;
                        //devolvemos la última id insertada
                        jsonresponse2["data"] = jsonresponse;
                        callback(null,jsonresponse2);

                    }
                });
            }
        });
    }
}

//delete Ranking
dbModel.deleteRanking = function(userData,callback)
{
    if (connection) 
    {
        var sqlExists = 'DELETE FROM ranking WHERE id = ' + connection.escape(userData.idRanking);
        connection.query(sqlExists, function(err, row) 
        {
            if(row.affectedRows == 0)
            {
                jsonresponse = {
                    tag : "deleteRanking",
                    success : false,
                    error : true, 
                    code : 400,
                    msg : "Ranking not found"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
            else
            {   
                jsonresponse = {
                    tag : "deleteRanking",
                    success : true,
                    error : false, 
                    code : 201,
                    msg : "Ranking deleted"
                };

                jsonresponse2 = {
                    data : []
                };

                jsonresponse2["data"] = jsonresponse;
                callback(null,jsonresponse2);
            }
        });
    }
}


//exportamos el objeto para tenerlo disponible en la zona de rutas
module.exports = dbModel;